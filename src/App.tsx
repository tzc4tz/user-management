import './styles/App.scss';
import {Routes, Route, BrowserRouter} from 'react-router-dom';
import UserProvider from 'context/user-context';
import {routes} from 'routes/routes';
import {Suspense} from 'react';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Suspense fallback={<p> Loading...</p>}>
          <UserProvider>
            <Routes>
              {routes.map((route, index) => (
                <Route key={index} {...route} />
              ))}
            </Routes>
          </UserProvider>
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;
