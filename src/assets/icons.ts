import {ReactComponent as SearchIcon} from './icons/search.svg';
import {ReactComponent as CameraIcon} from './icons/camera.svg';
import {ReactComponent as EditIcon} from './icons/edit.svg';
import {ReactComponent as ApproveIcon} from './icons/approve.svg';
import {ReactComponent as CheckIcon} from './icons/checked.svg';
import {ReactComponent as UnCheckIcon} from './icons/close.svg';
import {ReactComponent as PlusIcon} from './icons/plus.svg';
import {ReactComponent as TrashIcon} from './icons/trash.svg';

export {
  SearchIcon,
  CameraIcon,
  EditIcon,
  ApproveIcon,
  CheckIcon,
  UnCheckIcon,
  PlusIcon,
  TrashIcon,
};
