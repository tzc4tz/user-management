import {FormItemProps, FormValues} from 'types';
import styles from './form.module.scss';

const FormItem = ({
  name,
  label,
  type = 'text',
  register,
  required = false,
}: FormItemProps) => {
  return (
    <div className={styles['form__block']}>
      <label htmlFor={name}>{label} :</label>
      <input {...register(name as keyof FormValues, {required})} type={type} />
    </div>
  );
};

export default FormItem;
