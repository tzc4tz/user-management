import Upload from 'components/upload';
import {UserContext, UserContextInterface} from 'context/user-context';
import {useContext, useEffect, useState} from 'react';
import {useForm} from 'react-hook-form';
import {useNavigate, useParams} from 'react-router-dom';
import {FormValues} from 'types';

import styles from './form.module.scss';
import FormItem from './formItem';

const Form = () => {
  const navigate = useNavigate();
  const params = useParams();
  const {userHandler, getUser} = useContext(
    UserContext,
  ) as UserContextInterface;
  const [form, setForm] = useState({} as FormValues);
  const {register, handleSubmit, setValue, reset} = useForm<FormValues>({
    defaultValues: form,
  });

  useEffect(() => {
    const id = params?.['*'];
    if (!id) return;

    try {
      const user = getUser(id);
      setForm({...user});
      reset({...user});
    } catch (error) {
      console.log(error);
    }
  }, [params]);

  const submitHandler = (values: FormValues) => {
    const id = params?.['*'];
    userHandler(values, id);
  };

  return (
    <form className={styles.form} onSubmit={handleSubmit(submitHandler)}>
      <FormItem name="firstName" label="name" register={register} required />
      <FormItem
        name="lastName"
        label="family name"
        register={register}
        required
      />
      <FormItem
        name="id"
        label="id no"
        register={register}
        type="number"
        required
      />
      <FormItem name="mobile" label="mobile" register={register} />
      <FormItem
        name="birthDate"
        label="birth date"
        register={register}
        type="date"
      />
      <FormItem name="address" label="address" register={register} />
      <Upload
        register={register}
        setValue={setValue}
        className={styles['form__block']}
        defaultValue={form.image}
      />
      <div className={styles['form__actions']}>
        <input
          className={styles['form__actions--submit']}
          type="submit"
          value="save"
        />
        <button
          className={styles['form__actions--cancel']}
          type="button"
          onClick={() => navigate('/')}
        >
          cancel
        </button>
      </div>
    </form>
  );
};

export default Form;
