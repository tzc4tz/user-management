import Search from 'components/search';
import styles from './main.module.scss';
import {useContext} from 'react';
import {UserContext, UserContextInterface} from 'context/user-context';
import UserCard from 'components/user-card';
import {UserInterface} from 'types';
import {PlusIcon, TrashIcon} from 'assets/icons';
import {useNavigate} from 'react-router-dom';

const Main = () => {
  const navigate = useNavigate();
  const {usersList, pinAllHandler, pinAll, removeallUsers} = useContext(
    UserContext,
  ) as UserContextInterface;

  return (
    <div className={styles.main}>
      <Search />
      {usersList.length > 0 && (
        <div className={styles['main__pin']}>
          {pinAll && (
            <TrashIcon
              onClick={removeallUsers}
              width={16}
              height={16}
              fill="red"
            />
          )}
          <input
            checked={pinAll}
            type="checkbox"
            className={styles['main__selectAll']}
            onChange={pinAllHandler}
          />
        </div>
      )}
      <div className={styles['user-list']}>
        {usersList.map((user: UserInterface) => (
          <UserCard key={user.id} user={user} />
        ))}
      </div>
      <div
        onClick={() => navigate('/user-form')}
        className={styles['add--icon']}
      >
        <PlusIcon />
      </div>
    </div>
  );
};

export default Main;
