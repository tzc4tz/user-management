import React from 'react';

const Main = React.lazy(() => import('containers/main'));
const Approve = React.lazy(() => import('containers/approve'));
const Form = React.lazy(() => import('containers/form'));

export const routes = [
  {
    path: '/',
    element: <Main />,
  },
  {
    path: '/user-form/*',
    element: <Form />,
  },
  {
    path: '/check-user/*',
    element: <Approve />,
  },
];
